import os
import psycopg2

def db(event):
    conn = psycopg2.connect(
      host=os.environ["DB_HOST"],
      port=os.environ["DB_PORT"],
      database=os.environ["DB_NAME"],
      user=os.environ["DB_USERNAME"],
      password=os.environ["DB_PASSWORD"]
    )
    cursor = conn.cursor()

    cursor.execute('SELECT * FROM apollo_transactions LIMIT 5')

    result = cursor.fetchall()

    cursor.close()
    conn.close()

    response = {
      "status": "success",
      "body": str(result)
    }

    return response
