import boto3
import os
from datetime import datetime
import json

client = boto3.client('s3')

def update_file(event):
    key = event.get('key')

    result = client.get_object(
        Bucket=os.environ["S3_BUCKET"],
        Key=key
    )

    body = json.loads(result['Body'].read().decode('utf-8'))

    body['timestamp'] = datetime.now().strftime('%Y-%m-%d %H:%i:%s')

    client.put_object(
        Bucket=os.environ["S3_BUCKET"],
        Key=key,
        Body=json.dumps(body)
    )

    response = {
      "status": "success",
      "body": "created updated file"
    }

    return response
