def name(event):
    name = event.get('name')

    if (name == "error"):
      raise Exception("Invalid name")
     
    response = {
      "status": "success",
      "body": f"my name is {event.get('name')}"
    }

    return response
