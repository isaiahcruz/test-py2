import boto3
import os

client = boto3.client('s3')

def get_file(event):
    key = event.get('key')

    result = client.get_object(
        Bucket=os.environ["S3_BUCKET"],
        Key=key
    )

    body = result['Body'].read().decode('utf-8')

    response = {
      "status": "success",
      "body": body
    }

    return response
