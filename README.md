# Sample Serverless + Python

## Installation
#### 1. Install python
#### 2. Install nvm/node
#### 3. Install serverless
  ```bash
  npm i -g serverless
  ```
#### 4. Clone project and go to directory
#### 5. Install npm packages
  ```bash
  npm i
  ```
#### 4. Create python virtual environment
  ```bash
  python3 -m venv env
  source env/bin/activate
  ```
#### 5. Install requirements
  ```bash
  python3 -m pip install -r requirements.txt
  ```
#### 6. Create .env file
  ```bash
  cp .env.template .env
  ```
#### 7. Update .env value
#### 8. Test local invoke
  ```bash
  sls invoke local --function main --data '{ "action": "hello" }'

  sls invoke local --function main --data '{ "action": "name", "name": "Pie" }'

  sls invoke local --function main --data '{ "action": "db" }'
  ```
#### 9. Deploy using
  ```bash
  sls deploy
  ```

------
## Development
#### 1. Create python project using sls template
```bash
nvm install 18
nvm use 18
npm i -g serverless
sls create --template aws-python3 --path workshop-example-1 # change example to your name
cd workshop-example-1
sls invoke local --function hello
```

#### 2. Deploy sample template
##### a. Install sls plugin deployment-bucket
```bash
  sls plugin install -n serverless-deployment-bucket
```
##### b. Update `serverless.yml` `provider.region`
```yml
provider:
  ...
  region: ap-southeast-1
```
##### c. Set `deploymentBucket` config to `serverless.yml`
```yml
provider:
  ...
  deploymentBucket:
    name: moscord-serverless-deployment-bucket
    serverSideEncryption: AES256
```
##### d. Deploy
```bash
sls deploy
```
##### e. Test
```bash
aws lambda invoke --function-name=workshop-example-1-dev-hello result.json
```

#### 3. Create basic routing
##### a. Delete `handler.py`
##### b. Copy all the files in `src` folder except `db.py`
##### c. Test using local invoke
    ```bash
    sls invoke local --function main --data '{ "action": "name", "name": "Pie" }'
    ```

#### 4. Connecting to postgresql
##### a. Create and use virtual environment
  ```bash
  python3 -m venv env
  source env/bin/activate
  ```
##### b. Install sls plugin python-requirements
  ```bash
  sls plugin install -n serverless-python-requirements
  ```
##### c. Set `pythonRequirements` config to `serverless.yml` (root)
  ```yml
  custom:
    pythonRequirements:
      dockerizePip: non-linux
      slim: true
  ```
##### d. Install `psycopg2-binary`
  ```bash
  python3 -m pip install psycopg2-binary
  ```
##### e. Generate `requirements.txt`
  ```bash
  python3 -m pip freeze >> requirements.txt
  ```
##### f. Set `.env` values (include VPC for deployment)
##### g. Add environment configuration to `serverless.yml` `provider`
  ```yml
  provider:
    ...
    environment:
      DB_USERNAME: ${env:DB_USERNAME}
      DB_PASSWORD: ${env:DB_PASSWORD}
      DB_HOST: ${env:DB_HOST}
      DB_PORT: ${env:DB_PORT}
      DB_NAME: ${env:DB_NAME}
```
##### h. Add vpc configuration to `serverless.yml` `provider`
  ```yml
  provider:
    ...
    vpc:
      securityGroupIds:
        - ${env:VPC_SECGROUP_ID}
      subnetIds:
        - ${env:VPC_SUBNET_A}
        - ${env:VPC_SUBNET_B}
  ```
##### i. Copy `src/services/intro/db.py` and update `src/actions.py` with added import `db`
##### j. Local invoke
##### k. Deploy
##### l. Invoke aws lambda

#### 5. Using AWS SDK (boto3)
##### a. Install `boto3`
  ```bash
    python3 -m pip install boto3
  ```
##### b. Copy `.py` files related to s3 (*_file.py) and update actions.py
##### c. Set `.env` values for S3_BUCKET and add `provider.environment` to `serverless.yml` 
```yml
  provider:
    ...
    environment:
      ...
      S3_BUCKET = ${env:S3_BUCKET}
```
#### d. Add `provider.iam.role.statements` to `serverless.yml` 
  ```yml
  provider:
    ...
    iam:
      role:
        statements:
          - Effect: Allow
            Action:
              - 's3:GetObject'
              - 's3:PutObject'
            Resource: '*'
  ```
